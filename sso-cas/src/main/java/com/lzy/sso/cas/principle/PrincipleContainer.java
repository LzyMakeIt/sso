package com.lzy.sso.cas.principle;

import java.util.*;

/**
 * @Author: lzy
 * @Date: 2022/5/1 15:14
 * @Description: 容器 存放原生信息
 */
public class PrincipleContainer {
    Map<String,Object> info=new HashMap<>();
    Set<String> roles=new HashSet<>();

    /**
     * 根据认证授权构建Principle
     * @param auth
     * @param role
     * @return
     */
    protected Principle buildPrinciple(boolean auth,boolean role) {
        return new Principle(auth==false?null:info,role==false?null:roles);
    }

    /**
     * 添加基本信息
     * @param key
     * @param val
     */
    public void addInfo(String key, Object val){
        if(key==null)
            throw new NullPointerException();
        info.put(key,val);
    }

    /**
     * 添加角色信息
     * @param role
     */
    public void addRole(String role){
        if(role==null)
            throw new NullPointerException();
        roles.add(role);
    }

    /**
     * 批量添加
     * @param map
     */
    public void addInfos(Map<String,Object> map){
        if(map==null){
            return;
        }

        for(String k:map.keySet()){
            if(k!=null){
                info.put(k,map.get(k));
            }
        }
    }

    /**
     * 批量添加
     * @param collection
     */
    public void addRoles(Collection<String> collection){
        collection.forEach(item->{
            if(item!=null)
                roles.add(item);
        });
    }
}
