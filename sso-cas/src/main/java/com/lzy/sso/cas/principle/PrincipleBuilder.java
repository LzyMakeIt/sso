package com.lzy.sso.cas.principle;

import java.util.Map;
import java.util.Set;

/**
 * @Author: lzy
 * @Date: 2022/5/1 15:39
 * @Description: 指挥者与构建者
 */
public class PrincipleBuilder {
    // 是否通过认证
    private boolean auth=false;
    // 是否通过授权
    private boolean role=false;

    public Principle build(PrincipleContainer container){
        return container.buildPrinciple(auth,role);
    }
    public Principle build(Map<String,Object> info, Set<String> roles){
        return new Principle(info,roles);
    }

    /**
     * 认证成功
     */
    public void authSuccess() {
        this.auth = true;
    }

    public boolean isAuth() {
        return auth;
    }

    /**
     * 授权成功
     */
    public void roleSuccess() {
        this.role = true;
    }

    public boolean isRole() {
        return role;
    }
}
