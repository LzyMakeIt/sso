package com.lzy.sso.cas.matcher;

import com.lzy.sso.cas.pojo.Subject;

/**
 * @Author: lzy
 * @Date: 2022/5/1 14:36
 * @Description: 匹配器
 */
public interface Matcher {
    /**
     * 检查是否通过认证
     * @param target
     * @param subject
     * @return
     */
    boolean match(Subject target, Subject subject);
}
