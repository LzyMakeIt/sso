package com.lzy.sso.cas.cache;

import lombok.ToString;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: lzy
 * @Date: 2022/5/1 17:24
 * @Description: LRU缓存 待优化
 * 基于LinkedHashMap的LRU Cache并不是线程安全的 需要加锁
 * 将实现LRU的类包装 暴露常用的接口实现
 */

@ToString
public class LRUCache<T> {
    // Cache Size
    private int capacity;
    // Cache 实现
    private LRUMap<T> lruMap;

    // 先使用ReentrantLock 读写锁或根据ConcurrentHashMap优化以后再考虑
    private final Lock lock=new ReentrantLock();

    public LRUCache(){
        this(16);
    }

    public LRUCache(int capacity){
        this.capacity=capacity;
        lruMap=new LRUMap<>(capacity);
    }

    // 这里有线程安全问题 发生概率小 以后做优化
    public T get(String key){
        return lruMap.get(key);
    }

    /**
     * 以下操作都会导致线程安全问题 需要加锁包装
     * @param key
     * @param val
     * @return
     */
    public T put(String key,T val){
        try {
            lock.lock();
            return lruMap.put(key, val);
        }finally {
            lock.unlock();
        }
    }

    public T remove(String key){
        try {
            lock.lock();
            return lruMap.remove(key);
        } finally {
            lock.unlock();
        }
    }

    public boolean containsKey(String key){
        try {
            lock.lock();
            return lruMap.containsKey(key);
        }finally {
            lock.unlock();
        }
    }

    /**
     * 继承LinkedHashMap 天然的LRU实现
     * @param <T>
     */
    @ToString
    static class LRUMap<T> extends LinkedHashMap<String,T> {
        private int capacity;

        public LRUMap(int capacity){
            super(capacity,1.0f,true);
            this.capacity=capacity;
        }

        /**
         * 超过capacity时移除最久不访问的元素
         * @param eldest
         * @return
         */
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, T> eldest) {
            return this.size()>capacity;
        }
    }
}
