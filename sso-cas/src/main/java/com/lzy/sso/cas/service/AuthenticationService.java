package com.lzy.sso.cas.service;

import com.lzy.sso.cas.pojo.Subject;
import com.lzy.sso.cas.principle.Principle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @Author: lzy
 * @Date: 2022/4/25 16:39
 * @Description: 认证业务处理
 */

public interface AuthenticationService {
    boolean auth(String token); //认证
    Principle login(Subject subject); //登录
    void logout(HttpServletRequest request,HttpServletResponse response); //注销
    boolean isActive(HttpServletRequest request,HttpServletResponse response); //是否在登录状态
    Map<String,Object> getInfo(String token); //获取token对应的信息
    String processToken(HttpServletRequest request, HttpServletResponse response,Principle principle); //处理生成token
}
