package com.lzy.sso.cas.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: lzy
 * @Date: 2022/4/25 16:36
 * @Description: 通信消息实体
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message<T> {
    // 状态码
    private int code;

    // 信息
    private String msg;

    // 值
    private T val;
}
