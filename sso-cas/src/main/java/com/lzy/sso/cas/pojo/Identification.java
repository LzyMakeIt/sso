package com.lzy.sso.cas.pojo;

import com.lzy.sso.cas.principle.Principle;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: lzy
 * @Date: 2022/5/1 16:32
 * @Description: 认证信息
 */
@Data
@NoArgsConstructor
public class Identification {
    // 采用的方式 session | jwt
    private String type;

    // 认证以及授权的信息
    private Principle principle;

    // 特殊属性 如sessionID
    Map<String,Object> attributes=new HashMap<>();

    /**
     * 添加属性
     * @param name
     * @param value
     */
    public void addAttribute(String name,String value){
        attributes.put(name,value);
    }

    /**
     * 获取属性
     * @param name
     * @return
     */
    public Object getAttribute(String name){
        return attributes.get(name);
    }
}
