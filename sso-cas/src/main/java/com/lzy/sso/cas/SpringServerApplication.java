package com.lzy.sso.cas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: lzy
 * @Date: 2022/4/25 16:33
 * @Description: 启动器
 */

@SpringBootApplication
public class SpringServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringServerApplication.class);
    }
}
