package com.lzy.sso.cas.authenticator.impl;

import com.lzy.sso.cas.pojo.Subject;
import com.lzy.sso.cas.principle.Principle;
import com.lzy.sso.cas.authenticator.Authenticator;

/**
 * @Author: lzy
 * @Date: 2022/4/30 12:52
 * @Description: 模拟简单的登录 没什么用
 */
@Deprecated
public class DefaultAuthenticator implements Authenticator {

    @Override
    public Principle login(Subject subject) {
        return null;
    }
}
