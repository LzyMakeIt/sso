package com.lzy.sso.cas.authenticator;

import com.lzy.sso.cas.pojo.Subject;
import com.lzy.sso.cas.principle.PrincipleContainer;

/**
 * @Author: lzy
 * @Date: 2022/5/1 13:08
 * @Description: 用户自定义认证与授权操作 注入容器自动生效
 */
public abstract class Realm {
    /**
     * 认证操作
     * 待优化 Subject类不应该被用户直接操作
     * @param subject 接收到的登录请求的原型
     * @param principle 容器 放置个人信息 认证成功后个人信息生效
     * @return 真实的认证对象
     */
    public abstract Subject doGetAuthentication(Subject subject, PrincipleContainer principle);

    /**
     * 授权操作
     * @param subject 认证成功的Subject主体
     * @param principle 放置角色信息 授权完成后生效
     */
    public abstract void doGetAuthorization(Subject subject, PrincipleContainer principle);
}
