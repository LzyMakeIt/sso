package com.lzy.sso.cas.enable.processor;

import com.lzy.sso.cas.controller.AuthenticationController;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @Author: lzy
 * @Date: 2022/5/1 21:54
 * @Description: 保证controller注册前先初始化 以确保完成mapping的自适应
 */
public class AuthenticationBeanPostProcessor implements BeanPostProcessor, BeanFactoryAware {
    private BeanFactory beanFactory;
    private boolean init=false;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory=beanFactory;
    }

    /**
     * 优先初始化AuthenticationController
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(!init){
            init=true;
            beanFactory.getBean(AuthenticationController.class);

        }
        return null;
    }
}
