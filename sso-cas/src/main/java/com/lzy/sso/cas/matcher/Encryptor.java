package com.lzy.sso.cas.matcher;

/**
 * @Author: lzy
 * @Date: 2022/5/1 14:39
 * @Description: 加密器
 */
public interface Encryptor {
    /**
     * 完成对密码的加密
     * @param password
     * @return
     */
    public String encrypt(String password);
}
