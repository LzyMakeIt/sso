package com.lzy.sso.cas.generator;

/**
 * @Author: lzy
 * @Date: 2022/4/27 16:18
 * @Description: token生成器
 */
public interface TokenGenerator {
    /**
     * 生成token
     * @return
     */
    String generate();
}
