package com.lzy.sso.cas.generator.impl;

import com.lzy.sso.cas.generator.TokenGenerator;

import java.util.UUID;

/**
 * @Author: lzy
 * @Date: 2022/4/27 16:18
 * @Description: 使用UUID生成token
 */
public class UUIDTokenGenerator implements TokenGenerator {
    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
