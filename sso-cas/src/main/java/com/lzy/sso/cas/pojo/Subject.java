package com.lzy.sso.cas.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: lzy
 * @Date: 2022/4/25 16:40
 * @Description: 认证的主体 这个最好再修改一下 以防认证需要更多的参数
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subject {
    // 用户名
    private String username;

    // 密码
    private String password;
}
