package com.lzy.sso.cas.authenticator.impl;

import com.lzy.sso.cas.principle.PrincipleContainer;
import com.lzy.sso.cas.authenticator.Authorization;
import com.lzy.sso.cas.pojo.Subject;
import com.lzy.sso.cas.principle.Principle;

/**
 * @Author: lzy
 * @Date: 2022/4/30 22:41
 * @Description: 基于role-based access control实现的authorization authenticator
 */

//@Component
//@ConditionalOnProperty(value = "lzy.cas.cache",havingValue = "false")
public class RoleBasedAuthenticator extends Authorization {

    /**
     * 完成授权
     * @param subject
     * @param principle
     * @return
     */
    @Override
    public boolean acquireRole(Subject subject, PrincipleContainer principle) {
        realm.doGetAuthorization(subject,principle);
        return true;
    }

    /**
     * 打印日志
     * @param principle
     */
    @Override
    public void afterAuthentication(Principle principle) {
        System.out.println(principle);
    }
}
