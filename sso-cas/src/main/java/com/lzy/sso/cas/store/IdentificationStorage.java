package com.lzy.sso.cas.store;

import com.lzy.sso.cas.pojo.Identification;
import com.lzy.sso.cas.principle.Principle;
import com.lzy.sso.cas.principle.PrincipleBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * @Author: lzy
 * @Date: 2022/5/2 14:15
 * @Description: 存储于redis中的信息容器 与Identification对应
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdentificationStorage implements Serializable {
    private String type; // 类型
    private Map<String,Object> info; // 基本信息
    private Set<String> role; // 角色信息
    private Map<String,Object> attributes; // 特殊属性

    /**
     * 生成
     * @param identification
     */
    public IdentificationStorage(Identification identification){
        this.type=identification.getType();
        this.attributes=identification.getAttributes();

        Principle principle=identification.getPrinciple();
        this.info=principle.getAllBaseInfo();
        this.role=principle.getRoles();
    }

    /**
     * 获取操作对象
     * @return
     */
    public Identification convert(){
        Identification identification=new Identification();
        identification.setType(type);
        identification.setAttributes(attributes);
        identification.setPrinciple(new PrincipleBuilder().build(info,role));

        return identification;
    }
}
