package com.lzy.sso.cas.authenticator.impl;


import com.lzy.sso.cas.authenticator.Authenticator;
import com.lzy.sso.cas.pojo.Subject;
import com.lzy.sso.cas.principle.Principle;

/**
 * @Author: lzy
 * @Date: 2022/4/30 22:32
 * @Description: 装饰者
 * 添加一些新的访问权限控制 如黑名单
 * 下个版本再完善
 */
public class AccessControlAuthenticator implements Authenticator {
    private Authenticator authenticator;

    public AccessControlAuthenticator(Authenticator authenticator){
        this.authenticator=authenticator;
    }

    @Override
    public Principle login(Subject subject) {

        return null;
    }
}
