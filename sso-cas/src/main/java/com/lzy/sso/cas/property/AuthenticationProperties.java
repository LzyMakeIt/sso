package com.lzy.sso.cas.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author: lzy
 * @Date: 2022/4/27 11:23
 * @Description: 自动装配属性
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = AuthenticationProperties.AUTO_PROPERTY_PREFIX)
public class AuthenticationProperties {
    public static final String AUTO_PROPERTY_PREFIX="lzy.cas";

    // 认证请求处理的uri
    @Value("${lzy.cas.uri.auth:/auth}")
    private String authUri="/auth";

    // 登录请求处理的uri
    @Value("${lzy.cas.uri.login:/login}")
    private String loginUri="/login";

    // 注销请求处理的uri
    @Value("${lzy.cas.uri.logout:/logout}")
    private String logoutUri="/logout";

    // 登录页面路径
    @Value("${lzy.cas.page.login:/login.html}")
    private String loginPage="/login.html";

    // 登录成功的默认欢迎页
    @Value("${lzy.cas.page.welcome:/success.html}")
    private String welcomePage="/success.html";

    // 会话保持机制
    private String type="session";

    // token的保存方式
    @Value("${lzy.cas.session.store:local}")
    private String store="local";

    // 自定义cookie名称
    @Value("${spring.session.cookieName:SESSION}")
    private String cookieName="SESSION";
}
