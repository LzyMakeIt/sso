package com.lzy.sso.cas.utils;

import javax.servlet.http.Cookie;

/**
 * @Author: lzy
 * @Date: 2022/4/25 17:14
 * @Description:
 */
public class CookieUtils {

    /**
     * 获取cookie name对应的val
     * @param name
     * @param cookies
     * @return
     */
    public static String getCookieValue(String name,Cookie[] cookies){
        if(name==null)
            throw new NullPointerException("cookie name不能为空!");

        if(cookies==null){
            return null;
        }

        for(Cookie cookie:cookies){
            if(name.equals(cookie.getName())){
                return cookie.getValue();
            }
        }

        return null;
    }
}
