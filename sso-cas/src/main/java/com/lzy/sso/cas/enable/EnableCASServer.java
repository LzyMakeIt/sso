package com.lzy.sso.cas.enable;

import com.lzy.sso.cas.enable.selector.AuthenticationImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @Author: lzy
 * @Date: 2022/4/27 12:15
 * @Description: 开启认证中心服务
 * 第三方服务使用需要加此注解开启自动装配
 */
@Retention(RetentionPolicy.RUNTIME)
@Import({AuthenticationImportSelector.class})
public @interface EnableCASServer {
}
