package com.lzy.sso.cas.authenticator.impl;

import com.lzy.sso.cas.authenticator.Realm;
import com.lzy.sso.cas.pojo.Subject;
import com.lzy.sso.cas.principle.PrincipleContainer;

/**
 * @Author: lzy
 * @Date: 2022/5/1 15:52
 * @Description: 测试使用的默认Realm
 */

//@Component
public class DefaultRealm extends Realm {
    @Override
    public Subject doGetAuthentication(Subject subject, PrincipleContainer principle) {
        principle.addInfo("name",subject.getUsername());
        principle.addInfo("pwd",subject.getPassword());
        return new Subject("sso","sso");
    }

    @Override
    public void doGetAuthorization(Subject subject, PrincipleContainer principle) {
        principle.addRole("sso-admin");
    }
}
