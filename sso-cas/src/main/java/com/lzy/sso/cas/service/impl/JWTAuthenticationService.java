package com.lzy.sso.cas.service.impl;

import com.lzy.sso.cas.pojo.Identification;
import com.lzy.sso.cas.service.AbstractAuthenticationService;
import com.lzy.sso.cas.utils.CookieUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: lzy
 * @Date: 2022/4/27 16:06
 * @Description: JSON With Token
 */
public class JWTAuthenticationService extends AbstractAuthenticationService {
    /**
     * 注销
     * @param request
     * @param response
     */
    @Override
    public void logout(HttpServletRequest request,HttpServletResponse response) {
        throw new RuntimeException("jwt拒绝logout");
    }

    /**
     * 是否登录就以token存在为准
     * @param request
     * @param response
     * @return
     */
    @Override
    public boolean isActive(HttpServletRequest request, HttpServletResponse response) {
        String token= CookieUtils.getCookieValue("token",request.getCookies());
        return token==null?false:auth(token);
    }

    /**
     * 自定义信息
     * @param token
     * @return
     */
    @Override
    public Map<String, Object> getInfo(String token) {
        Map<String,Object> map=new HashMap<>();
        map.put("type","jwt");
        Identification identification = tokenManagement.getIdentification(token);

        map.put("info",identification.getPrinciple());
        return map;
    }

    /**
     * 设置为jwt就行
     * 客户端生成token用info就行
     * @param request
     * @param response
     * @param token
     * @param identification
     */
    @Override
    protected void doProcess(HttpServletRequest request, HttpServletResponse response, String token, Identification identification) {
        identification.setType("jwt");
    }
}
