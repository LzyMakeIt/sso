package com.lzy.sso.cas.authenticator;

import com.lzy.sso.cas.pojo.Subject;
import com.lzy.sso.cas.principle.Principle;

/**
 * @Author: lzy
 * @Date: 2022/4/30 12:43
 * @Description: 验证器 负责认证用户
 */
public interface Authenticator {
    /**
     * 登录
     * @param subject 认证主体
     * @return 是否通过认证
     */
    Principle login(Subject subject);
}
