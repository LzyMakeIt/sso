package com.lzy.sso.cas.matcher.impl;

import com.lzy.sso.cas.matcher.Encryptor;
import com.lzy.sso.cas.matcher.Matcher;
import com.lzy.sso.cas.pojo.Subject;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: lzy
 * @Date: 2022/5/1 14:37
 * @Description: 默认认证器
 */

//@Component
public class DefaultMatcher implements Matcher {
    @Autowired
    private Encryptor encryptor; // 加密器

    /**
     * 使用加密器加密后对比密码
     * @param target 真实主题
     * @param subject 认证主体
     * @return 是否匹配成功
     */
    @Override
    public boolean match(Subject target, Subject subject) {
        if(subject==null||subject.getPassword()==null||subject.getUsername()==null)
            return false;

        if(target==null||target.getPassword()==null||target.getUsername()==null){
            return false;
        }

        return target.getUsername().equals(subject.getUsername())
                &&target.getPassword().equals(
                        encryptor==null?subject.getPassword():
                                encryptor.encrypt(subject.getPassword()));
    }
}
