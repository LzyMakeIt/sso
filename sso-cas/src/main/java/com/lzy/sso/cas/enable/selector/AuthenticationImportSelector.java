package com.lzy.sso.cas.enable.selector;

import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Author: lzy
 * @Date: 2022/4/27 15:00
 * @Description:
 */
public class AuthenticationImportSelector implements DeferredImportSelector {
    /**
     * 根据配置导入相关组件
     * @param importingClassMetadata
     * @return
     */
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.lzy.sso.cas.config.AuthenticationAutoConfiguration","com.lzy.sso.cas.enable.processor.AuthenticationBeanPostProcessor"};
    }
}
