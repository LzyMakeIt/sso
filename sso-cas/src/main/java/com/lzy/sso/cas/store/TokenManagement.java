package com.lzy.sso.cas.store;

import com.lzy.sso.cas.pojo.Identification;

/**
 * @Author: lzy
 * @Date: 2022/4/26 20:09
 * @Description: token管理器
 */
public interface TokenManagement {
    boolean exists(String token); // 是否存在该token 并有效
    void addToken(String token, Identification identification, long expires); // 添加token
    void addToken(String token, Identification identification); // 添加token
    Identification getIdentification(String token); // 获取token对应的信息
}
