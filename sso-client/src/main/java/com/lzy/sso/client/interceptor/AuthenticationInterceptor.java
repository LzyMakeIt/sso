package com.lzy.sso.client.interceptor;

import com.lzy.sso.client.process.ProcessHandler;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: lzy
 * @Date: 2022/4/29 19:40
 * @Description:
 */

public class AuthenticationInterceptor implements HandlerInterceptor {

    private ProcessHandler processHandler;

    public AuthenticationInterceptor(ProcessHandler processHandler){
        this.processHandler=processHandler;
    }

    /**
     * 是否登录 没有登录则跳转到登录页面
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //模拟拦截
        boolean flag=false;
        if(processHandler.validate(request,response)){
            flag=true;
        }

        if(!flag){
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/auth?pageUrl=http://" + request.getServerName() + ":" + request.getServerPort() + request.getRequestURI());
            requestDispatcher.forward(request,response);
            return false;
        }

        return true;
    }
}
