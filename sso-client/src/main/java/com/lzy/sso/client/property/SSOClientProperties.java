package com.lzy.sso.client.property;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author: lzy
 * @Date: 2022/4/30 20:13
 * @Description:
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(SSOClientProperties.PROPERTY_PREFIX)
public class SSOClientProperties {
    public static final String PROPERTY_PREFIX="lzy.sso-client";

    // cas地址
    @Value("${lzy.cas.url}")
    private String casUrl;

    // 会话保持机制
    private String type="session";

    // 保存token的header名称
    private final String jwtName="Authorization";

    // jwt加密的秘钥
    @Value("${lzy.sso-client.jwt.secret}")
    private String jwtSecret="jwt secret";
}
