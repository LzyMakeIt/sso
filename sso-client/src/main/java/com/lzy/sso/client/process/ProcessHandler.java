package com.lzy.sso.client.process;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: lzy
 * @Date: 2022/4/30 19:12
 * @Description:
 */
public interface ProcessHandler {
    /**
     * 处理返回的token
     * @param request
     * @param response
     * @param token
     * @throws Exception
     */
    void process(HttpServletRequest request, HttpServletResponse response, String token) throws Exception;

    /**
     * 验证是否登录
     * @param request
     * @param response
     * @return
     */
    boolean validate(HttpServletRequest request,HttpServletResponse response);


}
