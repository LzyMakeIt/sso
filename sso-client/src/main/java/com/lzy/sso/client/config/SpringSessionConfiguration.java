package com.lzy.sso.client.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.RedisHttpSessionConfiguration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * @Author: lzy
 * @Date: 2022/4/29 19:17
 * @Description:
 */

@Configuration
public class SpringSessionConfiguration {
    /**
     * cookie自定义
     * @return
     */
    @Bean
    @ConditionalOnClass(RedisHttpSessionConfiguration.class)
    @ConditionalOnMissingBean(CookieSerializer.class)
    public CookieSerializer cookieSerializer(){
        DefaultCookieSerializer cookieSerializer=new DefaultCookieSerializer();
        cookieSerializer.setCookieName("SESSIONID");
        return cookieSerializer;
    }
}
