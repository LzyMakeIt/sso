package com.lzy.sso.client.config;

import com.lzy.sso.client.interceptor.AuthenticationInterceptor;
import com.lzy.sso.client.process.ProcessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: lzy
 * @Date: 2022/4/25 21:46
 * @Description:
 */

@Configuration
public class WebMVCConfiguration implements WebMvcConfigurer {
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET","POST","HEAD","DELETE","PUT","OPTIONS")
                .allowedHeaders("*")
                .maxAge(3600)
                .allowCredentials(false);
    }

    @Autowired
    private ProcessHandler processHandler;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthenticationInterceptor(processHandler))
                .order(0)
                .addPathPatterns("/**")
                .excludePathPatterns("/auth","/process");
    }
}
