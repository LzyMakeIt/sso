package com.lzy.sso.client.controller;

import com.lzy.sso.client.process.ProcessHandler;
import com.lzy.sso.client.property.SSOClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author: lzy
 * @Date: 2022/4/25 19:20
 * @Description:
 */

@Controller
public class AuthenticationController {
    @Autowired
    private SSOClientProperties properties;

    @RequestMapping("/auth")
    public String auth(HttpServletRequest request, @RequestParam("pageUrl") String pageUrl){
        return "redirect:"+properties.getCasUrl()+"/auth?pageUrl=http://"+request.getServerName()+":"+request.getServerPort()+"/process&showPage="+pageUrl;
    }

    @Autowired
    private ProcessHandler processHandler;

    @RequestMapping("/process")
    public String process(@RequestParam("showPage") String showPage,
                          @RequestParam("token") String token,
                          HttpServletRequest request,
                          HttpServletResponse response){
        try {
            processHandler.process(request,response,token);
            return "redirect:"+showPage;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "error";
    }
}
