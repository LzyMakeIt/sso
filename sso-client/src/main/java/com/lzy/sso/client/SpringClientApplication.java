package com.lzy.sso.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @Author: lzy
 * @Date: 2022/4/25 19:17
 * @Description:
 */

@EnableRedisHttpSession
@SpringBootApplication
public class SpringClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringClientApplication.class);
    }
}
