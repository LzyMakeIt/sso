package com.lzy.sso.client.process.impl;

import com.lzy.sso.client.process.AbstractProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @Author: lzy
 * @Date: 2022/4/30 19:11
 * @Description:
 */

@Primary
@Service
@ConditionalOnProperty(value = "lzy.sso-client.type",havingValue = "session")
public class SessionService extends AbstractProcessService {
    @Autowired
    private CookieSerializer cookieSerializer;

    @Override
    protected void processResult(HttpServletRequest request, HttpServletResponse response, Map map) {
        System.out.println(map);
        if ((Integer) map.get("code")==200){
            Map val =(Map) map.get("val");
            String sessionID =(String) val.get("sessionID");
            //保持相同的序列化方式
            cookieSerializer.writeCookieValue(new CookieSerializer.CookieValue(request,response,sessionID));
        }
    }

    @Override
    public boolean validate(HttpServletRequest request, HttpServletResponse response) {
        return request.getSession(false)!=null;
    }
}
