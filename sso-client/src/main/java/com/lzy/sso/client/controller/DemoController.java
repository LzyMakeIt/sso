package com.lzy.sso.client.controller;

import com.lzy.sso.client.process.impl.JWTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: lzy
 * @Date: 2022/4/25 19:24
 * @Description: 模拟真实的接口
 */

@Controller
public class DemoController {

    @Autowired(required = false)
    private JWTService jwtService;

    @GetMapping("/name")
    public String getName(){
        return "forward:/hello.html";
    }

    @ResponseBody
    @GetMapping("/sign")
    public Map<String, Object> getSign(){
        Map<String,Object> map=new HashMap<>();
        map.put("id",1);
        map.put("name","jack");
        map.put("role","teacher");

        return map;
    }
}
