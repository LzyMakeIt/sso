package com.lzy.sso.client.config;

import com.lzy.sso.client.property.SSOClientProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: lzy
 * @Date: 2022/4/30 20:27
 * @Description:
 */

@Configuration
@EnableConfigurationProperties(SSOClientProperties.class)
public class SSOClientConfiguration {

}
