### 介绍

SSO单点登录框架  
CAS无侵入式、天然分布式支持  
基于原生Spring的良好扩展性  
用户自定义认证授权方式  
可配置基于Session或JSON With Token两种会话管理机制  
Session方式实现基于Spring Session共享机制，采用Redis做Session存储中心  
JWT方式正在完善 实现同时支持Session与JWT 预计下个版本推出  


### CAS架构图
![输入图片说明](sso-cas/cas%E6%9E%B6%E6%9E%84%E5%9B%BE.png)


### SSO时序图
[https://www.yuque.com/docs/share/03c6a784-aefc-4fec-8a1c-cfe50f69f2e3?# 《SSO-时序图》
链接有效期至 2022-05-05 21:09:24](http://)


### 接口说明

#### 一、服务端接口说明
1. GET  /login   登录请求处理
2. GET  /auth    验证携带token的合法性
3. POST /auth    拿取token置换相关信息
4. POST /logout  注销请求处理

#### 二、客户端接口说明
1. GET|POST /auth    若不存在登录状态则此接口负责认证
2. GET|POST /process 完成认证后跳转到的处理接口


### 核心组件

#### SSO-CAS服务端
 **一、功能组件** 
1. Authenticator   认证器   -- 核心认证接口
2. Authorization   权限获取   -- 定义模板方法，子类实现具体授权功能
3. Realm           自定义认证授权   -- 用户实现注入容器实现自定义
4. Matcher         匹配器   -- 负责密码的匹配
5. Encryptor       加密器   -- 负责密码加密
6. Principle       信息容器   -- 认证成功后获取的信息容器 保存用户基本信息与角色信息
7. TokenManagement 密令管理器   -- 负责管理认证成功后生成的Token与Principle
8. TokenGenerator  密令生成器   -- 负责生成Token
9. AuthenticationService 业务执行   -- 规范业务核心方法
10. AuthenticationController 处理器   -- SpringMVC控制器 处理请求 可配置的URI接口

 **二、启动组件** 
1. AuthenticationImportSelector   组件导入器   -- 导入启动组件
2. AuthenticationProperties   自动装配属性   -- 获取并保存配置的属性
3. AuthenticationAutoConfiguration   自动装配   -- 根据配置文件自动装配组件
4. AuthenticationBeanPostProcessor   后置处理器   -- 提前完成AuthenticationController的初始化

 **三、其他组件** 
1. LRUCache         缓存   -- LRU缓存
2. PrincipleContainer 信息容器   -- 暴露对象添加Principle信息
3. PrincipleBuilder 建造者  -- 负责建造Principle
4. Identification   认证信息   -- 包含认证类型、Principle与其他信息
5. IdentificationStorage 认证信息序列化实体   -- 序列化存储在其他介质

#### SSO-Client客户端
 **一、基础组件** 
1. SSOClientProperties 自动装配属性   -- 获取保存配置的属性
2. AuthenticationController 处理器   -- 提供核心认证与Token处理接口
3. ProcessHandler    Token处理器   -- 负责置换信息并根据会话类型进行处理
4. AbstractProcessService Token处理器   -- 提供模板方法，使用HttpClient置换信息子类实现信息处理
5. AuthenticationInterceptor 认证拦截器   -- 聚合ProcessHandler，对需认证的接口请求拦截转发


### 使用说明

#### 一、starter方式启动
1. 使用Maven install添加sso-cas到本地仓库，引入相关坐标到Web项目中。
2. 主启动类添加@EnableCASServer注解。
3. 可根据配置类动态注入、包装完善新功能。

#### 二、二次开发
1. git clone到本地。
2. 自定义相关实现。

### 注意: 
1. 开发时client与Server端要采用相同的Cookie序列化方式，以防Session获取失败。
2. 客户端发送请求URL请携带认证成功的处理接口和展示页面的路由。URL参数分别为pageUrl与showPage。
3. 目前不支持Client端starter启动。
4. 详情可参考sso-client处理方式。


### 核心配置
#### 一、服务端配置
采用哪种方式 Session 或 JSON With Token
```
lzy.cas.type = session | jwt
```

若采用session方式配置 可选择sessionID的存储位置 local或redis
```
lzy.cas.session.store = redis | local
```

可以自定义认证、登录与注销请求的URI
```
lzy.cas.uri.auth = /auth
lzy.cas.uri.login = /login
lzy.cas.uri.logout = /logout
```

自定义登录页面、认证成功后的默认展示页面（如果没有指定showPage）

```
lzy.cas.page.login = /login.html
lzy.cas.page.welcome = /success.html
```

分布式Session使用redis做存储中心 若不选择则session不需配置
```
spring.session.store-type = redis
spring.redis.url = redis://server:port
```
保存全局Session Cookie的名称
```
spring.session.cookieName=SESSION
```

#### 二、客户端配置
CAS认证中心的地址
```
lzy.cas.url=http://server.com
```

配置客户端使用的会话方式 注意与服务端保持一致
```
lzy.sso-client.type=session | jwt
```

分布式Session需配置Redis服务
```
spring.session.store-type=redis
spring.redis.url=redis://server:port
```

JWT加密使用的私钥
```
lzy.sso-client.jwt.secret=jwt secret
```

 **最后：** 对于session相关等更多属性等待下个版本